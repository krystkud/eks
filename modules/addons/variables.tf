variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "Repository"  = lookup(var.globals, "Repository", "")
    "Builder"     = lookup(var.globals, "Builder", "")
    "Module"      = "TFM-eks-modules-addons-${var.MODULE_VERSION}"
    "CreatedBy"   = "terraform"
  }
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "CLUSTER_NAME" {
  type        = string
  description = "Cluster name"
}

variable "CLUSTER_ADDONS" {
  description = "Map of cluster addon configurations to enable for the cluster. Addon name can be the map keys or set with `name`"
  type        = map(any)
  default     = {}
}

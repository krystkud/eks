variable "MODULE_VERSION" {
  default     = "2.0.8"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "Repository"  = lookup(var.globals, "Repository", "")
    "Builder"     = lookup(var.globals, "Builder", "")
    "Module"      = "TFM-eks-modules-fargate_nodes-${var.MODULE_VERSION}"
    "CreatedBy"   = "terraform"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "CLUSTER_NAME" {
  type        = string
  description = "Cluster name"
}

variable "SUBNET_IDS" {
  description = "Provide list of the subnet ids to launch resources in"
  type        = list(string)
}

variable "FARGATE_NAMESPACE" {
  type        = string
  description = "Kubernetes namespace for selection"
}

variable "LABELS" {
  type        = map(string)
  description = "Key-value mapping of Kubernetes labels for selection"
  default     = {}
}

variable "SUFFIX" {
  type        = string
  description = "Optional Cluster Suffix, allows to create multiple clusters in the same project"
  default     = ""
}

variable "DEPENDS_ON_MODULE" {
  description = "Provide module dependency"
  type        = any
  default     = null
}

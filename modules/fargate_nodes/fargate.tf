data "aws_iam_policy_document" "fargate_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks-fargate-pods.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "fargate" {
  name               = "${local.environment_name}-fargate-role${var.SUFFIX}"
  assume_role_policy = data.aws_iam_policy_document.fargate_role.json
  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-cluster${var.SUFFIX}",
      "Object", "aws_eks_cluster",
      "kubernetes.io/cluster/${var.CLUSTER_NAME}", "owned",
      "k8s.io/cluster/${var.CLUSTER_NAME}", "owned",
      "Namespace", var.FARGATE_NAMESPACE
    )
  )
}

resource "aws_iam_role_policy_attachment" "attachment_fargate" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = aws_iam_role.fargate.name
}

resource "aws_eks_fargate_profile" "fargate" {
  cluster_name           = var.CLUSTER_NAME
  fargate_profile_name   = "${local.environment_name}-fargate-profile${var.SUFFIX}"
  pod_execution_role_arn = aws_iam_role.fargate.arn
  subnet_ids             = var.SUBNET_IDS

  selector {
    namespace = var.FARGATE_NAMESPACE
    labels    = var.LABELS
  }

  timeouts {
    create = "30m"
    delete = "30m"
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-cluster${var.SUFFIX}",
      "Object", "aws_eks_cluster",
      "kubernetes.io/cluster/${var.CLUSTER_NAME}", "owned",
      "k8s.io/cluster/${var.CLUSTER_NAME}", "owned",
      "Namespace", var.FARGATE_NAMESPACE
    )
  )
  depends_on = [var.DEPENDS_ON_MODULE]
}

output "arn" {
  value       = aws_eks_node_group.node_group.arn
  description = "Amazon Resource Name (ARN) of the EKS Node Group"
}

output "id" {
  value       = aws_eks_node_group.node_group.id
  description = "EKS Cluster name and EKS Node Group name separated by a colon (:)"
}

output "resources" {
  value       = aws_eks_node_group.node_group.resources
  description = "List of objects containing information about underlying resources"
}
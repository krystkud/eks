resource "aws_launch_template" "eks_node" {
  name                   = "${local.environment_name}-eks-node-group${var.SUFFIX}"
  image_id               = data.aws_ami.eks_node.id
  instance_type          = var.INSTANCE_TYPE
  ebs_optimized          = true
  vpc_security_group_ids = compact(concat([var.NESSUS_SG], var.SECURITY_GROUPS_ID, [var.cluster["NodeGroupSecurityGroupId"]], [var.networking["DefaultSecurityGroupId"]]))
  user_data              = base64encode(local.userdata)

  dynamic "instance_market_options" {
    for_each = var.ENABLE_SPOT_INSTANCES == false ? [] : list(var.ENABLE_SPOT_INSTANCES)
    content {
      market_type = "spot"
      spot_options {
        spot_instance_type = "one-time"
        max_price          = var.SPOT_MAX_PRICE
      }
    }
  }

  metadata_options {
    http_endpoint               = var.EKS_NODE_METADATA_HTTP_ENDPOINT
    http_tokens                 = var.EKS_NODE_METADATA_HTTP_TOKENS
    http_put_response_hop_limit = var.EKS_NODE_METADATA_HTTP_PUT_RESPONSE_HOP_LIMIT
  }

  block_device_mappings {
    device_name = data.aws_ami.eks_node.root_device_name

    ebs {
      volume_size = var.VOLUME_SIZE
      volume_type = var.VOLUME_TYPE
      encrypted   = true
      kms_key_id  = var.KMS_KEY_ID
      iops        = var.VOLUME_TYPE == "io1" ? var.VOLUME_SIZE * 50 : null
    }
  }

  tag_specifications {
    resource_type = "volume"

    tags = merge(
      local.default_tags,
      var.CUSTOM_TAGS,
      map(
        "Name", "${local.environment_name}-eks-group${var.SUFFIX}",
        "Object", "aws_ebs_volume"
      )
    )
  }

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      local.default_tags,
      var.CUSTOM_TAGS,
      map(
        "Name", "${local.environment_name}-eks-group${var.SUFFIX}",
        "Object", "aws_ebs_volume"
      )
    )
  }

  depends_on = [var.DEPENDS_ON_MODULE]

  tags = merge(
    local.default_tags,
    var.CUSTOM_TAGS,
    map(
      "Name", "${local.environment_name}-eks-group${var.SUFFIX}"
    )
  )
  #checkov:skip=CKV_AWS_79 Metadata is needed for node-group to register in the cluster
}

resource "aws_eks_node_group" "node_group" {
  cluster_name    = var.cluster["Name"]
  node_group_name = var.NODE_GROUP_NAME
  node_role_arn   = aws_iam_role.eks_group_role.arn
  subnet_ids      = var.SUBNET_IDS
  labels          = var.LABELS

  scaling_config {
    desired_size = var.NR_OF_NODES
    max_size     = var.MAX_NR_OF_NODES != null ? var.MAX_NR_OF_NODES : (var.NR_OF_NODES + 1)
    min_size     = var.MIN_NR_OF_NODES != null ? var.MIN_NR_OF_NODES : var.NR_OF_NODES
  }

  launch_template {
    id      = aws_launch_template.eks_node.id
    version = aws_launch_template.eks_node.latest_version
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  depends_on = [var.DEPENDS_ON_MODULE, aws_iam_role.eks_group_role]

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-node-group-${var.NODE_GROUP_NAME}",
      "Object", "aws_eks_node_group"
    )
  )
}

##
# Grand access to kms key
##
resource "aws_kms_grant" "autoscaling" {
  name              = "${local.environment_name}-grant-for-node-group-autoscaling"
  key_id            = var.KMS_KEY_ID
  grantee_principal = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
  operations        = ["Encrypt", "Decrypt", "GenerateDataKey", "CreateGrant", "ReEncryptFrom", "ReEncryptTo", "DescribeKey", "GenerateDataKeyWithoutPlaintext", "RetireGrant"]
}

##
# Grand access to shared ami kms key
##
resource "aws_kms_grant" "autoscaling_shared" {
  count             = local.kms_shared_ami_key_id != "" ? 1 : 0
  name              = "${local.environment_name}-grant-for-node-group-autoscaling-shared"
  key_id            = local.kms_shared_ami_key_id
  grantee_principal = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
  operations        = ["Encrypt", "Decrypt", "RetireGrant", "DescribeKey", "GenerateDataKey", "GenerateDataKeyWithoutPlaintext", "ReEncryptFrom", "ReEncryptTo", "CreateGrant"]
}
##
# Attach alb_target_group to EKS node group
##
# resource "aws_autoscaling_attachment" "autoscaling_attachment" {
#   count = var.USE_TARGET_GROUP == true ? 1 : 0
#   # https://github.com/aws/containers-roadmap/issues/709
#   autoscaling_group_name = lookup(lookup(lookup(aws_eks_node_group.node_group, "resources")[0], "autoscaling_groups")[0], "name")
#   alb_target_group_arn   = var.TARGET_GROUP_ARN
# }

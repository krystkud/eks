terraform {
  required_version = ">= 0.12.0"

  required_providers {
    # This is the minimal version of the provider(s) required by this module to work properly
    # Update it only if changes in this module really required features provided by this version
    # Don't update blindly as we will use latest anyway.
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.2"
    }
  }
}

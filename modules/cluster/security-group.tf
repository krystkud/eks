##
# Define eks cluster security group
##
resource "aws_security_group" "cluster" {
  name        = "${local.environment_name}-cluster-default${var.SUFFIX}"
  vpc_id      = var.networking["VpcId"]
  description = "Communication between the control plane and worker node groups"

  tags = merge(
    local.default_tags,
    var.CLUSTER_SG_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-cluster-default${var.SUFFIX}",
      "Object", "aws_security_group",
      "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${var.SUFFIX}", "owned"
    )
  )
}

##
# Define eks node security group
##
resource "aws_security_group" "eks_node" {
  name        = "${local.environment_name}-eks-node-default${var.SUFFIX}"
  description = "Communication between the control plane and worker nodes"
  vpc_id      = var.networking["VpcId"]

  tags = merge(
    local.default_tags,
    var.CLUSTER_NODE_SG_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-eks-node-default${var.SUFFIX}",
      "Object", "aws_launch_template",
      "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${var.SUFFIX}", "owned"
    )
  )
}

##
# Define eks_node security rules
##
resource "aws_security_group_rule" "eks_node_ingress_1025_65535_flow" {
  type                     = "ingress"
  description              = "Allow worker nodes to communicate with control plane (kubelet and workload TCP ports)"
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_node.id
  source_security_group_id = aws_security_group.cluster.id
}

resource "aws_security_group_rule" "eks_node_ingress_443" {
  type                     = "ingress"
  description              = "Allow worker nodes to communicate with control plane (workloads using HTTPS port, commonly used with extension API servers)"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_node.id
  source_security_group_id = aws_security_group.cluster.id
}

resource "aws_security_group_rule" "eks_node_ingress_0" {
  type              = "ingress"
  description       = "Allow worker nodes to communicate with each other (all ports)"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  self              = true
  security_group_id = aws_security_group.eks_node.id
}

resource "aws_security_group_rule" "eks_node_ingress_53_udp" {
  type              = "ingress"
  description       = "Allow DNS access to worker nodes in group inside VPC (UDP port)"
  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  self              = true
  security_group_id = aws_security_group.eks_node.id
}

resource "aws_security_group_rule" "eks_node_ingress_53_tcp" {
  type              = "ingress"
  description       = "Allow DNS access to worker nodes in group inside VPC (TCP port)"
  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.eks_node.id
}

resource "aws_security_group_rule" "eks_node_egress_cluster" {
  type              = "egress"
  description       = "Allow worker nodes to communicate with each other (all ports)"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  self              = true
  security_group_id = aws_security_group.eks_node.id
}

##
# eks_master appends eks_node flow
##
resource "aws_security_group_rule" "eks_master_ingress_443" {
  type                     = "ingress"
  description              = "Allow control plane to receive API requests from worker nodes"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.eks_node.id
  security_group_id        = aws_security_group.cluster.id
}

resource "aws_security_group_rule" "eks_master_egress_1025_65535" {
  type                     = "egress"
  description              = "Allow control plane to communicate with worker nodes (kubelet and workload TCP ports)"
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.eks_node.id
  security_group_id        = aws_security_group.cluster.id
}

resource "aws_security_group_rule" "eks_master_egress_443" {
  type                     = "egress"
  description              = "Allow control plane to communicate with worker nodes (workloads using HTTPS port, commonly used with extension API servers)"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.eks_node.id
  security_group_id        = aws_security_group.cluster.id
}

resource "local_file" "kubeconfig" {
  count                = var.CREATE_KUBECONFIG == true ? 1 : 0
  content              = local.kubeconfig
  filename             = var.KUBECONFIG_FILENAME
  file_permission      = 0644
  directory_permission = 0755
}

resource "null_resource" "validate_cluster" {
  count = var.ENDPOINT_PUBLIC_ACCESS == true ? 1 : 0
  # Changes to any instance of the cluster requires re-provisioning
  triggers = {
    cluster_id = aws_eks_cluster.cluster.id
  }

  depends_on = [aws_eks_cluster.cluster]

  provisioner "local-exec" {
    command = <<EOF

echo "Check if ${aws_eks_cluster.cluster.endpoint} is up and running";
STATUS=$( curl --max-time 5 --insecure --write-out %%{http_code} --silent --output /dev/null ${aws_eks_cluster.cluster.endpoint} || echo );
while [ $STATUS -ne 403 ]; do
  sleep 5;
  STATUS=$( curl --max-time 5 --insecure --write-out %%{http_code} --silent --output /dev/null ${aws_eks_cluster.cluster.endpoint} || echo );
done;
EOF
  }
}

resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name      = "aws-auth"
    namespace = "kube-system"
    labels = merge(
      local.default_tags,
      map(
        "Name", "${local.environment_name}-cluster${var.SUFFIX}",
        "Object", "aws_eks_cluster"
      )
    )
  }

  data = {
    mapRoles = local.aws_auth_roles
    mapUsers = local.aws_auth_users
  }

  depends_on = [
    null_resource.validate_cluster,
    aws_eks_cluster.cluster,
    aws_security_group.cluster,
    local_file.kubeconfig
  ]
}

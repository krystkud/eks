variable "MODULE_VERSION" {
  default     = "2.0.8"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "Repository"  = lookup(var.globals, "Repository", "")
    "Builder"     = lookup(var.globals, "Builder", "")
    "CreatedBy"   = "terraform"
    "Module"      = "TFM-eks-modules-cluster-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"

  kubeconfig = <<KUBECONFIG
---
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: ${aws_eks_cluster.cluster.certificate_authority[0].data}
    server: ${aws_eks_cluster.cluster.endpoint}
  name: ${aws_eks_cluster.cluster.arn}
contexts:
- context:
    cluster: ${aws_eks_cluster.cluster.arn}
    user: ${aws_eks_cluster.cluster.arn}
  name: ${aws_eks_cluster.cluster.arn}
current-context: ${aws_eks_cluster.cluster.arn}
users:
- name: ${aws_eks_cluster.cluster.arn}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws
      env:
        - name: AWS_STS_REGIONAL_ENDPOINTS
          value: regional
      args:
        - eks
        - --region
        - ${local.region}
        - get-token
        - --cluster-name
        - ${aws_eks_cluster.cluster.name}

KUBECONFIG

  aws_auth_roles = <<ROLES
- rolearn: ${aws_iam_role.eks_node_role.arn}
  username: system:node:{{EC2PrivateDNSName}}
  groups:
    - system:bootstrappers
    - system:nodes
%{if var.ENABLE_FARGATE == true~}
- rolearn: ${join("", aws_iam_role.fargate.*.arn)}
  username: system:node:{{SessionName}}
  groups:
    - system:bootstrappers
    - system:nodes
    - system:node-proxier
%{endif~}
%{for role in var.ADMIN_ROLES_ARN~}
- rolearn: ${role}
  username: ${split("/", role)[1]}
  groups:
    - system:masters
%{endfor}
%{for role, group in var.CUSTOM_ROLES_ARN~}
- rolearn: ${role}
  username: ${split("/", role)[1]}
  groups:
    - ${group}
%{endfor}
ROLES

  aws_auth_users = <<USERS
%{for user in var.ADMIN_USERS_ARN~}
- userarn: ${user}
  username: ${split("/", user)[1]}
  groups:
    - system:masters
%{endfor}
%{for user, group in var.CUSTOM_USERS_ARN~}
- userarn: ${user}
  username: ${split("/", user)[1]}
  groups:
    - ${group}
%{endfor}
USERS
}

##
# Define common networking variables
##
locals {
  cluster = {
    "ClusterSecurityGroupId"   = aws_security_group.cluster.id
    "Endpoint"                 = aws_eks_cluster.cluster.endpoint
    "CA"                       = aws_eks_cluster.cluster.certificate_authority[0].data
    "Name"                     = aws_eks_cluster.cluster.id
    "NodeProfileName"          = aws_iam_instance_profile.eks_node_profile.name
    "NodeRoleARN"              = aws_iam_role.eks_node_role.arn
    "NodeSecurityGroupId"      = aws_security_group.eks_node.id
    "NodeGroupSecurityGroupId" = aws_security_group.eks_group.id
    "KubernetesVersion"        = aws_eks_cluster.cluster.version
  }
  region = var.globals["Region"]
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "networking" {
  description = "Provide common networking variables for other module"
  type        = map(any)
}

variable "SUFFIX" {
  type        = string
  description = "Optional Cluster Suffix, allows to create multiple clusters in the same project"
  default     = ""
}

variable "SUBNET_IDS" {
  description = "Provide list of the subnet ids to launch resources in"
  type        = list(string)
}

variable "ENDPOINT_PUBLIC_ACCESS" {
  default     = true
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled"
  type        = bool
}

variable "PUBLIC_ACCESS_CIDRS" {
  description = "Provide cidr block which should have access to eks public endpoint"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "ENDPOINT_PRIVATE_ACCESS" {
  default     = true
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled"
  type        = bool
}

variable "CLUSTER_SG_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for EKS Cluster security group"
  type        = map(any)
}

variable "CLUSTER_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for EKS Cluster"
  type        = map(any)
}

variable "CLUSTER_NODE_SG_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for EKS Nodes security group"
  type        = map(any)
}

variable "KUBERNETES_VERSION" {
  default     = "1.18"
  description = "Provide Kubernetes master version"
  type        = string
}

variable "ADMIN_ROLES_ARN" {
  default     = []
  type        = list(string)
  description = "Provide list of the roles arn with admin right"
}

variable "ADMIN_USERS_ARN" {
  default     = []
  type        = list(string)
  description = "Provide list of the users arn with admin right"
}

variable "CUSTOM_USERS_ARN" {
  default     = {}
  type        = map(string)
  description = "Provide a map of custom users ARNs with corresponding K8S RBAC groups."
}

variable "CUSTOM_ROLES_ARN" {
  default     = {}
  type        = map(string)
  description = "Provide a map of custom roles ARNs with corresponding K8S RBAC groups."
}

variable "KUBECONFIG_FILENAME" {
  default     = "kubeconfig.conf"
  type        = string
  description = "Provide kubeconfig filename"
}

variable "CREATE_KUBECONFIG" {
  description = "Define whether or not kubeconfig file shall be created"
  type        = bool
  default     = true
}

variable "ENCRYPTION_RESOURCES" {
  description = "List of strings with resources to be encrypted"
  type        = list(string)
  default     = ["secrets"]
}

variable "KMS_KEY_ID" {
  description = "Provide kms key id used to encrypt root partition"
  type        = string
}

variable "NESSUS_SG" {
  default     = ""
  description = "Nessus Scanner Security group ID. Used for pentesting in PROD/STG envs. See Nessus module"
  type        = string
}

variable "ENABLED_CLUSTER_LOG_TYPES" {
  default     = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  description = "A list of the desired control plane logging to enable (CloudWatch). Possible values are api, audit, authenticator, controllerManager, scheduler"
  type        = list(string)
}

variable "ENABLE_FARGATE" {
  default     = false
  type        = bool
  description = "Set to true to enable Fargate profiles"
}

variable "FARGATE_NAMESPACE" {
  type        = string
  description = "Kubernetes namespace for selection, used with Fargate only"
  default     = "kube-system"
}

variable "LABELS" {
  type        = map(string)
  description = "Key-value mapping of Kubernetes labels for selection"
  default     = {}
}

variable "CLOUDWATCH_LOGS_RETENTION" {
  description = "Retention for CW logs, in days. Only useful if some ENABLED_CLUSTER_LOG_TYPES are enabled"
  default     = null
  type        = number
}

data "aws_iam_policy_document" "fargate_role" {
  count = var.ENABLE_FARGATE == true ? 1 : 0
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks-fargate-pods.amazonaws.com", "eks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "fargate" {
  count              = var.ENABLE_FARGATE == true ? 1 : 0
  name               = "${local.environment_name}-fargate-role${var.SUFFIX}"
  assume_role_policy = join("", data.aws_iam_policy_document.fargate_role.*.json)
  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-cluster${var.SUFFIX}",
      "Object", "aws_eks_cluster",
      "kubernetes.io/cluster/${aws_eks_cluster.cluster.id}", "owned",
      "k8s.io/cluster/${aws_eks_cluster.cluster.id}", "owned",
      "Namespace", var.FARGATE_NAMESPACE
    )
  )
}

resource "aws_iam_role_policy_attachment" "attachment_fargate" {
  count      = var.ENABLE_FARGATE == true ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = join("", aws_iam_role.fargate.*.name)
}

resource "aws_eks_fargate_profile" "fargate" {
  count                  = var.ENABLE_FARGATE == true ? 1 : 0
  cluster_name           = aws_eks_cluster.cluster.id
  fargate_profile_name   = "${local.environment_name}-fargate-profile${var.SUFFIX}"
  pod_execution_role_arn = join("", aws_iam_role.fargate.*.arn)
  subnet_ids             = var.SUBNET_IDS

  selector {
    namespace = var.FARGATE_NAMESPACE
    labels    = var.LABELS
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-cluster${var.SUFFIX}",
      "Object", "aws_eks_cluster",
      "kubernetes.io/cluster/${aws_eks_cluster.cluster.id}", "owned",
      "k8s.io/cluster/${aws_eks_cluster.cluster.id}", "owned",
      "Namespace", var.FARGATE_NAMESPACE
    )
  )
}

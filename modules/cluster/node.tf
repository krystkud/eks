##
# Define eks node role
##
data "aws_iam_policy_document" "eks_node_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "ec2.amazonaws.com",
      ]
    }

    effect = "Allow"
  }
}

resource "aws_iam_role" "eks_node_role" {
  name               = "${local.environment_name}-eks-node-role${var.SUFFIX}"
  assume_role_policy = data.aws_iam_policy_document.eks_node_policy_document.json
}

resource "aws_iam_role_policy_attachment" "eks_node_attach_eks_workernode_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_node_role.name
  depends_on = [aws_iam_role.eks_node_role]
}

resource "aws_iam_role_policy_attachment" "eks_node_attach_eks_cni_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_node_role.name
  depends_on = [aws_iam_role.eks_node_role]
}

resource "aws_iam_role_policy_attachment" "eks_node_attach_ec2_container_registry_readonly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_node_role.name
  depends_on = [aws_iam_role.eks_node_role]
}

resource "aws_iam_role_policy_attachment" "eks_node_attach_cloudwatch_readonly_access" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
  role       = aws_iam_role.eks_node_role.name
  depends_on = [aws_iam_role.eks_node_role]
}

resource "aws_iam_instance_profile" "eks_node_profile" {
  name       = "${local.environment_name}-eks-node-profile${var.SUFFIX}"
  role       = aws_iam_role.eks_node_role.name
  depends_on = [aws_iam_role.eks_node_role]
}

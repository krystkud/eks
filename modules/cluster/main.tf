##
# Define eks cluster role
##
data "aws_iam_policy_document" "eks_cluster_policy_document" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"

      identifiers = [
        "eks.amazonaws.com"
      ]
    }

    effect = "Allow"
  }
}

resource "aws_iam_role" "cluster_role" {
  name               = "${local.environment_name}-cluster-role${var.SUFFIX}"
  assume_role_policy = data.aws_iam_policy_document.eks_cluster_policy_document.json
}

resource "aws_iam_role_policy_attachment" "cluster_attach_eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster_role.name
  depends_on = [aws_iam_role.cluster_role]
}

resource "aws_iam_role_policy_attachment" "cluster_attach_eks_service_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.cluster_role.name
  depends_on = [aws_iam_role.cluster_role]
}

resource "aws_eks_cluster" "cluster" {
  name                      = "${local.environment_name}-cluster${var.SUFFIX}"
  role_arn                  = aws_iam_role.cluster_role.arn
  version                   = var.KUBERNETES_VERSION
  enabled_cluster_log_types = var.ENABLED_CLUSTER_LOG_TYPES #tfsec:ignore:AWS067

  vpc_config {
    endpoint_public_access  = var.ENDPOINT_PUBLIC_ACCESS #tfsec:ignore:AWS069
    public_access_cidrs     = var.PUBLIC_ACCESS_CIDRS
    endpoint_private_access = var.ENDPOINT_PRIVATE_ACCESS
    security_group_ids      = compact([aws_security_group.cluster.id, var.NESSUS_SG])
    subnet_ids              = var.SUBNET_IDS
  }

  encryption_config {
    resources = var.ENCRYPTION_RESOURCES #tfsec:ignore:AWS066
    provider {
      key_arn = var.KMS_KEY_ID
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster_attach_eks_cluster_policy,
    aws_iam_role_policy_attachment.cluster_attach_eks_service_policy,
    aws_cloudwatch_log_group.cluster_logs[0]
  ]

  tags = merge(
    local.default_tags,
    var.CLUSTER_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-cluster${var.SUFFIX}",
      "Object", "aws_eks_cluster"
    )
  )
  #checkov:skip=CKV_AWS_39:Public access to cluster must be limited to our ip addresses
  #checkov:skip=CKV_AWS_37:Logging is optional, it should be added on prod env
}

resource "aws_cloudwatch_log_group" "cluster_logs" {
  count             = var.ENABLED_CLUSTER_LOG_TYPES == null ? 0 : 1
  name              = "/aws/eks/${local.environment_name}-cluster${var.SUFFIX}/cluster"
  retention_in_days = var.CLOUDWATCH_LOGS_RETENTION


  tags = merge(
    local.default_tags,
    map(
      "Name", "/aws/eks/${local.environment_name}-cluster${var.SUFFIX}/cluster",
      "Object", "aws_cloudwatch_log_group"
    )
  )
}

output "endpoint" {
  description = "Return eks cluster endpoint"
  value       = aws_eks_cluster.cluster.endpoint
}

output "cluster_ca_certificate" {
  description = "Return eks ca certificate (base64)"
  value       = base64decode(aws_eks_cluster.cluster.certificate_authority[0].data)
}

output "cluster_oidc_issuer" {
  description = "OIDC issuer associated with the cluster"
  value       = aws_eks_cluster.cluster.identity[0].oidc[0].issuer
}

output "arn" {
  description = "Return eks cluster arn"
  value       = aws_eks_cluster.cluster.arn
}

output "name" {
  description = "Return eks cluster name"
  value       = aws_eks_cluster.cluster.id
}

output "cluster_security_group_id" {
  description = "Return eks cluster security group id"
  value       = aws_security_group.cluster.id
}

output "node_security_group_id" {
  description = "Return eks node security group id"
  value       = aws_security_group.eks_node.id
}

output "node_role_arn" {
  description = "Return eks node role arn"
  value       = aws_iam_role.eks_node_role.arn
}

output "node_role_name" {
  description = "Return k8s node role name"
  value       = aws_iam_role.eks_node_role.name
}

output "node_profile_name" {
  description = "Return eks node profile name"
  value       = aws_iam_instance_profile.eks_node_profile.name
}

output "kubeconfig" {
  description = "Return eks kubeconfig"
  sensitive   = true
  value       = local.kubeconfig
}

output "kubeconfig_filename" {
  description = "Return kubeconfig filename"
  value       = var.KUBECONFIG_FILENAME
}

output "cluster" {
  description = "Return common networking variables used by other modules"
  value       = local.cluster
}

output "metadata" {
  value       = kubernetes_config_map.aws_auth.metadata
  description = "K8S Config Map info"
}

output "eks_fargate_profile_role_arn" {
  description = "ARN of the EKS Fargate Profile IAM role"
  value       = join("", aws_iam_role.fargate.*.arn)
}

output "eks_fargate_profile_role_name" {
  description = "Name of the EKS Fargate Profile IAM role"
  value       = join("", aws_iam_role.fargate.*.name)
}

output "eks_fargate_profile_id" {
  description = "EKS Cluster name and EKS Fargate Profile name separated by a colon"
  value       = join("", aws_eks_fargate_profile.fargate.*.id)
}

output "eks_fargate_profile_arn" {
  description = "Amazon Resource Name (ARN) of the EKS Fargate Profile"
  value       = join("", aws_eks_fargate_profile.fargate.*.arn)
}

output "eks_fargate_profile_status" {
  description = "Status of the EKS Fargate Profile"
  value       = join("", aws_eks_fargate_profile.fargate.*.status)
}

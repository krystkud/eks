##
# Define custom EKS node security rules
##
resource "aws_security_group_rule" "eks_node_ingress_custom" {
  count             = length(var.CUSTOM_INGRESS_ACCESS)
  description       = "Allow incoming custom flow on port ${split(":", var.CUSTOM_INGRESS_ACCESS[count.index])[1]}/tcp"
  type              = "ingress"
  from_port         = split(":", var.CUSTOM_INGRESS_ACCESS[count.index])[1]
  to_port           = split(":", var.CUSTOM_INGRESS_ACCESS[count.index])[1]
  protocol          = "tcp"
  cidr_blocks       = [split(":", var.CUSTOM_INGRESS_ACCESS[count.index])[0]]
  security_group_id = var.cluster["NodeSecurityGroupId"]
}

resource "aws_security_group_rule" "eks_node_egress_custom" {
  count             = length(var.CUSTOM_EGRESS_ACCESS)
  description       = "Allow outgoing custom flow on port ${split(":", var.CUSTOM_EGRESS_ACCESS[count.index])[1]}/tcp"
  type              = "egress"
  from_port         = split(":", var.CUSTOM_EGRESS_ACCESS[count.index])[1]
  to_port           = split(":", var.CUSTOM_EGRESS_ACCESS[count.index])[1]
  protocol          = "tcp"
  cidr_blocks       = [split(":", var.CUSTOM_EGRESS_ACCESS[count.index])[0]]
  security_group_id = var.cluster["NodeSecurityGroupId"]
}

resource "aws_security_group_rule" "eks_node_ingress_custom_from_sg" {
  count                    = length(var.SOURCE_INGRESS_SECURITY_GROUPS)
  description              = "Allow incoming custom network flow on port ${split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[1]}/tcp."
  type                     = "ingress"
  from_port                = split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[1]
  to_port                  = split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[1]
  protocol                 = "tcp"
  source_security_group_id = split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[0]
  security_group_id        = var.cluster["NodeSecurityGroupId"]
}

resource "aws_security_group_rule" "eks_node_egress_custom_to_sg" {
  count                    = length(var.DESTINATION_EGRESS_SECURITY_GROUPS)
  description              = "Allow outgoing custom network flow on port ${split(":", var.DESTINATION_EGRESS_SECURITY_GROUPS[count.index])[1]}/tcp."
  type                     = "egress"
  from_port                = split(":", var.DESTINATION_EGRESS_SECURITY_GROUPS[count.index])[1]
  to_port                  = split(":", var.DESTINATION_EGRESS_SECURITY_GROUPS[count.index])[1]
  protocol                 = "tcp"
  source_security_group_id = split(":", var.DESTINATION_EGRESS_SECURITY_GROUPS[count.index])[0]
  security_group_id        = var.cluster["NodeSecurityGroupId"]
}

variable "MODULE_VERSION" {
  default     = "2.0.8"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "Repository"  = lookup(var.globals, "Repository", "")
    "Builder"     = lookup(var.globals, "Builder", "")
    "Module"      = "TFM-eks-modules-nodes-${var.MODULE_VERSION}"
    "CreatedBy"   = "terraform"
  }
  environment_name      = "${var.globals["Project"]}-${var.globals["Environment"]}"
  kms_shared_ami_key_id = var.globals["KmsSharedAmiKeyId"]
}

data "aws_caller_identity" "current" {}

data "aws_ami" "eks_node" {
  most_recent = true

  filter {
    name   = "name"
    values = ["EKS-Worker-Ubuntu-${var.KUBERNETES_VERSION}-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = [var.globals["AmiOwner"] == "" ? data.aws_caller_identity.current.account_id : var.globals["AmiOwner"]]
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "cluster" {
  description = "Provide eks cluster variables from eks module"
  type        = map(any)
}

variable "SECURITY_GROUPS_ID" {
  description = "Provide id of the security groups"
  type        = list(string)
}

variable "SUBNET_IDS" {
  description = "Provide list of the subnet ids to launch resources in"
  type        = list(string)
}

variable "TARGET_GROUP_ARNS" {
  description = "Provide additional node target group arns"
  type        = list(string)
  default     = []
}

variable "INSTANCE_TYPE" {
  description = "Provide kubernetes node instance type"
  default     = "t3.medium"
  type        = string
}

variable "NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = null
  type        = number
}

variable "MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = null
  type        = number
}

variable "KMS_KEY_ID" {
  description = "Provide kms key id used to encrypt root partition"
  type        = string
}

variable "VOLUME_SIZE" {
  description = "Provide size of the root partition in GB"
  default     = 20
  type        = number
}

variable "VOLUME_TYPE" {
  description = "Provide type of the root partition"
  default     = "gp2"
  type        = string
}

variable "ENABLE_SPOT_INSTANCES" {
  default     = false
  description = "This option enable spot instances"
  type        = bool
}

variable "SPOT_MAX_PRICE" {
  default     = 0.048
  description = "Max spot price"
  type        = number
}

variable "SUFFIX" {
  description = "Provide pool suffix name if node module is used multiple times"
  default     = ""
  type        = string
}

variable "CLUSTER_SUFFIX" {
  description = "Provide cluster suffix if it has been used in eks cluster module"
  default     = ""
  type        = string
}

variable "ASG_HEALTH_CHECK_TYPE" {
  default     = "EC2"
  description = "Provide health check type for node ASG [ELB or EC2]"
  type        = string
}

variable "ASG_HEALTH_CHECK_GRACE_PERIOD" {
  default     = 60
  type        = number
  description = "Provide health check grace period for node ASG."
}

variable "ASG_COOLDOWN_PERIOD" {
  default     = 300
  description = "Provide cooldown period for node ASG."
  type        = number
}

variable "KUBELET_EXTRA_ARGS" {
  default     = ""
  type        = string
  description = "Provide additional kubelet args"
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "Provide additional ssh public keys"
  default     = []
  type        = list(string)
}

variable "DEPENDS_ON_MODULE" {
  description = "Provide module dependency"
  type        = any
  default     = null
}

variable "NESSUS_SG" {
  default     = ""
  description = "Nessus Scanner Security group ID. Used for pentesting in PROD/STG envs. See Nessus module"
  type        = string
}

variable "KUBERNETES_VERSION" {
  default     = "1.18"
  description = "Provide Kubernetes master version"
  type        = string
}

variable "CUSTOM_TAGS" {
  default     = {}
  type        = map(string)
  description = "Optional map of custom tags for EC2 resources. Example: {HYPNOS=IGNORE}"
}

variable "DOCKER_REGISTRY_CREDENTIALS" {
  description = "Provide list of docker registries with credentials."
  default     = []
  type        = list(string)
}

variable "SOURCE_INGRESS_SECURITY_GROUPS" {
  type        = list(string)
  default     = []
  description = "Provide a list of additional source security groups with port, which should be allowed for ingress traffic to nodes. Example: ['sg-3123123123:80', 'sg-3123123123:443']"
}

variable "DESTINATION_EGRESS_SECURITY_GROUPS" {
  type        = list(string)
  default     = []
  description = "Provide a list of additional destination security groups with port, which should be allowed for egress traffic from nodes. Example: ['sg-3123123123:80', 'sg-3123123123:443']"
}

variable "CUSTOM_INGRESS_ACCESS" {
  description = "Provide cidr and port list block which should be opened to ingress nodes (ex ['1.1.1.1/32:443', '1.1.1.1/32:80'])"
  default     = []
  type        = list(string)
}

variable "CUSTOM_EGRESS_ACCESS" {
  description = "Provide cidr and port list block which should have access to ingress nodes (ex ['1.1.1.1/32:443', '1.1.1.1/32:80'])"
  default     = []
  type        = list(string)
}

variable "ASG_CUSTOM_TAGS" {
  type        = list(map(string))
  description = "Optional map of custom tags for ASG resources. Example: [{key = HYPNOS, value = IGNORE, propagate_at_launch = true}]"
  default     = []
}

variable "CRON_JOBS_START_HOURS" {
  type        = number
  default     = 1
  description = "Define from which hour in UTC daily, weekly, monthly jobs should start (time will be randomized between START - STOP inclusive)"
}

variable "CRON_JOBS_END_HOURS" {
  type        = number
  default     = 6
  description = "Define to which hour in UTC daily, weekly, monthly jobs should start (time will be randomized between START - STOP inclusive)"
}

variable "EKS_NODE_METADATA_HTTP_ENDPOINT" {
  type        = string
  description = "Determines if HTTP endpoint for metadata is enabled or disabled Example: 'enabled'"
  default     = "enabled"
}

variable "EKS_NODE_METADATA_HTTP_TOKENS" {
  type        = string
  description = "Whether or not the metadata service requires session tokens: either optional or required"
  #tfsec:ignore:GEN001
  default = "optional"
}

variable "EKS_NODE_METADATA_HTTP_PUT_RESPONSE_HOP_LIMIT" {
  type        = number
  description = "Response hop limit which has to be configured to higher value if PODs need to access EC2 instance metadata."
  default     = 1
}

variable "DOCKER_LOG_ROTATION_ENABLED" {
  type        = bool
  description = "True if docker containers logs should be rotated"
  default     = true
}

variable "DOCKER_LOG_ROTATION_DRIVER" {
  type        = string
  description = "Docker log driver to be used"
  default     = "json-file"
}

variable "DOCKER_LOG_ROTATION_MAX_SIZE" {
  type        = string
  description = "Docker log file max size"
  default     = "10m"
}

variable "DOCKER_LOG_ROTATION_MAX_FILE" {
  type        = number
  description = "Docker log file max number of files"
  default     = 10
}

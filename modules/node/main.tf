resource "random_string" "key" {
  length  = 4
  special = false
  upper   = false
  number  = false

  keepers = {
    environment_name = local.environment_name
  }
}
resource "aws_key_pair" "eks_node" {
  count      = var.globals["PublicKeyPath"] != "" ? 1 : 0
  key_name   = "${local.environment_name}-eks-node-${random_string.key.result}${var.SUFFIX}"
  public_key = chomp(file(var.globals["PublicKeyPath"]))
}

##
# Define eks-node launch template
##
resource "aws_launch_template" "eks_node" {
  name          = "${local.environment_name}-eks-node${var.SUFFIX}"
  image_id      = data.aws_ami.eks_node.id
  instance_type = var.INSTANCE_TYPE
  dynamic "instance_market_options" {
    for_each = var.ENABLE_SPOT_INSTANCES == false ? [] : list(var.ENABLE_SPOT_INSTANCES)
    content {
      market_type = "spot"
      spot_options {
        spot_instance_type = "one-time"
        max_price          = var.SPOT_MAX_PRICE
      }
    }
  }

  metadata_options {
    http_endpoint               = var.EKS_NODE_METADATA_HTTP_ENDPOINT
    http_tokens                 = var.EKS_NODE_METADATA_HTTP_TOKENS
    http_put_response_hop_limit = var.EKS_NODE_METADATA_HTTP_PUT_RESPONSE_HOP_LIMIT
  }

  ebs_optimized                        = true
  instance_initiated_shutdown_behavior = "terminate"

  vpc_security_group_ids = compact(concat([var.NESSUS_SG], var.SECURITY_GROUPS_ID, [var.cluster["NodeSecurityGroupId"]]))

  user_data = base64encode(local.userdata)

  iam_instance_profile {
    name = var.cluster["NodeProfileName"]
  }

  key_name = var.globals["PublicKeyPath"] != "" ? aws_key_pair.eks_node[0].key_name : null

  block_device_mappings {
    device_name = data.aws_ami.eks_node.root_device_name

    ebs {
      volume_size = var.VOLUME_SIZE
      volume_type = var.VOLUME_TYPE
      encrypted   = true
      kms_key_id  = var.KMS_KEY_ID
      iops        = var.VOLUME_TYPE == "io1" ? var.VOLUME_SIZE * 50 : null
    }
  }

  tag_specifications {
    resource_type = "volume"

    tags = merge(
      local.default_tags,
      var.CUSTOM_TAGS,
      map(
        "Name", "${local.environment_name}-eks-node${var.SUFFIX}",
        "Object", "aws_ebs_volume"
      )
    )
  }

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      local.default_tags,
      var.CUSTOM_TAGS,
      map(
        "Name", "${local.environment_name}-eks-node${var.SUFFIX}",
        "Object", "aws_ebs_volume"
      )
    )
  }

  depends_on = [var.DEPENDS_ON_MODULE]

  tags = merge(
    local.default_tags,
    var.CUSTOM_TAGS,
    map(
      "Name", "${local.environment_name}-eks-node${var.SUFFIX}"
    )
  )
  #checkov:skip=CKV_AWS_79 Metadata is needed for node to register in the cluster
}

##
# Define eks-node autoscaling group
##
resource "aws_autoscaling_group" "eks_node" {
  name_prefix = "${local.environment_name}-eks-node${var.SUFFIX}-"

  timeouts {
    delete = "60m"
  }

  launch_template {
    id      = aws_launch_template.eks_node.id
    version = aws_launch_template.eks_node.latest_version
  }

  vpc_zone_identifier       = var.SUBNET_IDS
  min_size                  = var.MIN_NR_OF_NODES != null ? var.MIN_NR_OF_NODES : var.NR_OF_NODES
  desired_capacity          = var.NR_OF_NODES
  max_size                  = var.MAX_NR_OF_NODES != null ? var.MAX_NR_OF_NODES : (var.NR_OF_NODES + 1)
  health_check_grace_period = var.ASG_HEALTH_CHECK_GRACE_PERIOD
  health_check_type         = var.ASG_HEALTH_CHECK_TYPE
  default_cooldown          = var.ASG_COOLDOWN_PERIOD
  force_delete              = false
  wait_for_capacity_timeout = 0
  target_group_arns         = var.TARGET_GROUP_ARNS

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  tags = concat([
    map("key", "Environment", "value", local.default_tags["Environment"], "propagate_at_launch", true),
    map("key", "Project", "value", local.default_tags["Project"], "propagate_at_launch", true),
    map("key", "CreatedBy", "value", "terraform", "propagate_at_launch", true),
    map("key", "Name", "value", "${local.environment_name}-eks-node${var.SUFFIX}", "propagate_at_launch", true),
    map("key", "SshUser", "value", var.globals["SshUser"], "propagate_at_launch", true),
    map("key", "Object", "value", "aws_autoscaling_group", "propagate_at_launch", true),
    map("key", "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${var.CLUSTER_SUFFIX}", "value", "shared", "propagate_at_launch", true),
    ],
    var.ASG_CUSTOM_TAGS
  )
}

##
# Grand access to kms key
##
resource "aws_kms_grant" "autoscaling" {
  name              = "${local.environment_name}-grant-for-autoscaling"
  key_id            = var.KMS_KEY_ID
  grantee_principal = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
  operations        = ["Encrypt", "Decrypt", "GenerateDataKey", "CreateGrant", "ReEncryptFrom", "ReEncryptTo", "DescribeKey", "GenerateDataKeyWithoutPlaintext", "RetireGrant"]
  depends_on        = [aws_autoscaling_group.eks_node]
}

##
# Grand access to shared ami kms key
##
resource "aws_kms_grant" "autoscaling_shared" {
  count             = local.kms_shared_ami_key_id != "" ? 1 : 0
  name              = "${local.environment_name}-grant-for-autoscaling-shared"
  key_id            = local.kms_shared_ami_key_id
  grantee_principal = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
  operations        = ["Encrypt", "Decrypt", "RetireGrant", "DescribeKey", "GenerateDataKey", "GenerateDataKeyWithoutPlaintext", "ReEncryptFrom", "ReEncryptTo", "CreateGrant"]
  depends_on        = [aws_autoscaling_group.eks_node]
}

## 2.1.0 2022-03-21
SIE-3413: Adopt EKS addons

## 2.0.14 2022-03-16
Expose cluster OIDC issuer as output

## 2.0.13 2022-01-18
GID-2599: Ensure kubeconfig is not destroyed before k8s resources

## 2.0.12 2021-09-29
TM-467: Added Jenkinsfile.

## 2.0.11 2021-08-03
TM-483: Fix ssh keys for node

## 2.0.10 2021-03-03
TM-413: Fix tag for node group

## 2.0.9 2021-03-03
TM-345 Enable docker log rotation

## 2.0.8 18-02-2021
TM-326: CD-450 Setup retention for EKS logs
TM-309: More meaningful tags in modules

## 2.0.6 2021-02-23
Changed AMI

## 2.0.5 2020-02-12
TM-320: Fixed wrong auth configuration in EKS cluster for custom roles.

## 2.0.4 2020-02-09
TM-291: Move checkov comments, fix example

## 2.0.3 2020-12-15
TM-257: Disable Metadata in EC2 instances

## 2.0.2 2020-12-10
TM-243: Change asg extra tags

## 2.0.1 2020-12-02
TM-223: TM-229 Calico AND Multi CIDR VPC
TM-234: EKS Node Groups

## 2.0.0 2020-11-06
TM-219: upgrade to terraform 13

## 1.0.9 4.11.2020
TM-219: Split modules to tf12 and tf13

## 1.0.8 2020-10-27
TM-200: Configure log rotation for docker containers

## 1.0.7 2020-10-27
TM-199: Update insance metadata option - put response hop limit

## 1.0.6 2020-10-23
TM-196: Cron on eks nodes can start in random h on different nodes

## 1.0.5 2020-10-19
TM-158: Fix eks labels on security groups

## 1.0.4 2020-10-06

TM-179 Correct suffix value in EKS

## 1.0.3 2020-10-12

TM-181: Add Hypnos ignore tags to EKS ASG

## 1.0.2 2020-10-05

TM-175: Allow usage of io1 block devices

## 1.0.1 2020-09-30

TM-153: ES-1176 Extra security groups


## 1.0.0 2020-09-28

TM-126: EKS release
TM-164: ES-1204 IAM users and roles may be added to EKS cluster with custom K8S RBAC roles.
TM-157: ES-1176 Docker credentials can be specified in EKS node module.
TM-17: fix tags  
TM-113: SSH keys are optional for nodes  
TM-17: Robots Tests  
TM-17: Lint and Fix Examples  
TM-17: fix merge  
TM-17: Better tests for EKS  
TM-92: Fix aws provider  
TM-68: CAT-1291 Add Ami Owner to EKS  
TM-00: Align with terraform-module-aws develop  
TM-17: Add fargate eks tests scenario and modify eks cluster to work with multiple example  
TM-42: Reorder examples and fix docs  
TM-17: CAT-1221 fargate poc  
CAT-1142: Ported EKS examples  
CAT-1140: auto-generated README for EKS module  
Add small fixes  
Fix url  
Readme fixes  
Initial comit  


# v0.X (unreleased)

Next (milestone) Release! There were 3 PR's merged, here's a highlight:

### Features / Enhancements

###fixes

### Contributors


# 1.0

### Features / Enhancements

- [CAT-310](https://jira.oberthur.com/browse/CAT-310) We need to migrate to terraform v0.12
- [CAT-149](https://jira.oberthur.com/browse/CAT-149) Change IAM policies to JSON documents
- [CAT-403](https://jira.oberthur.com/browse/CAT-403) Update plugin providers in terraform
- [CAT-137](https://jira.oberthur.com/browse/CAT-137) Simplify module usage
- [CAT-134](https://jira.oberthur.com/browse/CAT-134) Make autogenerated docs for terraform modules
- [CAT-456](https://jira.oberthur.com/browse/CAT-456) Add tags to eks cluster
- [CAT-395](https://jira.oberthur.com/browse/CAT-395) Fix readme
- [CAT-560](https://jira.oberthur.com/browse/CAT-560) Upgrade aws provider to v2.39.0
- [CAT-602](https://jira.oberthur.com/browse/CAT-602) Generate info about dependencies
- [CAT-677](https://jira.oberthur.com/browse/CAT-677) Unify source in the readme file
- [CAT-479](https://jira.oberthur.com/browse/CAT-479) Update AWS provider
- [CAT-230](https://jira.oberthur.com/browse/CAT-230) Move terraform provisioner versions to modules
- [CAT-214](https://jira.oberthur.com/browse/CAT-214) Create terraform module release v0.3
- [CAT-115](https://jira.oberthur.com/browse/CAT-115) Add initial ingress alb module
- [CAT-174](https://jira.oberthur.com/browse/CAT-174) Refactor security groups implementation
- [CAT-146](https://jira.oberthur.com/browse/CAT-146) Migrate example to README.md in usage section
- [CAT-713](https://jira.oberthur.com/browse/CAT-713) Upgrade AWS provider to v2.45.0
- [CAT-858](https://jira.oberthur.com/browse/CAT-858) EKS managed node group
- [CAT-944](https://jira.oberthur.com/browse/CAT-944) Migrate modules to new local.environment_name
- [CAT-945](https://jira.oberthur.com/browse/CAT-945) Configure custom kms key for eks data
- [CAT-861](https://jira.oberthur.com/browse/CAT-861) Fix type of variables
- [CAT-885](https://jira.oberthur.com/browse/CAT-885) Add support for extra ssh keys in eks node
- [CAT-863](https://jira.oberthur.com/browse/CAT-863) Add helm module
- [CAT-887](https://jira.oberthur.com/browse/CAT-887) Add eks project example
- [CAT-882](https://jira.oberthur.com/browse/CAT-882) Add ECS Fargate module
- [CAT-838](https://jira.oberthur.com/browse/CAT-838) Generate book for terraform modules
- [CAT-930](https://jira.oberthur.com/browse/CAT-930) Update Cognito modules to support temporary_password_validity_days
- [CAT-72](https://jira.oberthur.com/browse/CAT-72) Create terraform eks module
- [CAT-87](https://jira.oberthur.com/browse/CAT-87) EKS - validate_pods shall be divided in two

### Bugfixes

- [CAT-364](https://jira.oberthur.com/browse/CAT-364) Remove provider from modules
- [CAT-441](https://jira.oberthur.com/browse/CAT-441) Error during kubernetes-user-access setup
- [CAT-340](https://jira.oberthur.com/browse/CAT-340) Can't bootstrap cluster in different region
- [CAT-410](https://jira.oberthur.com/browse/CAT-410) The following providers do not have any version constraints
- [CAT-683](https://jira.oberthur.com/browse/CAT-683) Refresh eks module
- [CAT-597](https://jira.oberthur.com/browse/CAT-597) quoted references are now deprecated
- [CAT-647](https://jira.oberthur.com/browse/CAT-647) Escape > in terraform version
- [CAT-253](https://jira.oberthur.com/browse/CAT-253) Specifying a required terraform version
- [CAT-793](https://jira.oberthur.com/browse/CAT-793) EKS module doesn't work with default variables
- [CAT-849](https://jira.oberthur.com/browse/CAT-849) Add required field for terraform input variables
- [CAT-978](https://jira.oberthur.com/browse/CAT-978) Modules should define minimal version not strict version

### Contributors

Dawid Malinowski

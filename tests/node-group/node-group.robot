*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite applies examples from given example_path, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the given example
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot
Resource  resources/kubernetes.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/node-group
${region}        %{REGION}

*** Test Cases ***

Test simple
    Set Test Variable  ${example_path}  ${example_path}/simple
    ${local_ip}=  Get AWS visible IP
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}  var={'PUBLIC_ACCESS_CIDRS':[\'${local_ip}/32\']}

    ${terraform_resp}=  Terraform get json module values  ${example_path}  eks  aws_eks_cluster.cluster
    Set suite variable  ${name}  ${terraform_resp['values']['id']}

    AWSession.spawn  eks  ${region}
    ${aws_api_resp}=  AWSession.get  list_clusters
    should contain  ${aws_api_resp['clusters']}  ${name}

Test simple-destroy
    Set Test Variable  ${example_path}  ${example_path}/simple
    Terraform destroy  ${example_path}
    AWSession.spawn  eks  ${region}
    ${aws_api_resp}=  AWSession.get  list_clusters
    should not contain  ${aws_api_resp['clusters']}  ${name}

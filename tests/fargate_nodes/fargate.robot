*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples from modules repository, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot
Resource  resources/kubernetes.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/fargate_nodes
${region}        %{REGION}

*** Test Cases ***
Test fargate
    Set Test Variable  ${example_path}  ${example_path}/fargate
    ${local_ip}=  Get AWS visible IP
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}  var={'PUBLIC_ACCESS_CIDRS':[\'${local_ip}/32\']}

    ${terraform_resp}=  Terraform get json module values  ${example_path}  eks  aws_eks_cluster.cluster
    Set suite variable  ${eks_name}  ${terraform_resp['values']['id']}

    ${fargate_resp}=  Terraform get json module values  ${example_path}  fargate_default  aws_eks_fargate_profile.fargate
    Set suite variable  ${fargate}  ${fargate_resp['values']['fargate_profile_name']}

    AWSession.spawn  eks  ${region}
    ${aws_api_resp}=  AWSession.get  list_clusters
    should contain  ${aws_api_resp['clusters']}  ${eks_name}

    ${aws_api_resp}=  AWSession.get  list_fargate_profiles  clusterName=${eks_name}
    should contain  ${aws_api_resp['fargateProfileNames']}  ${fargate}

Test fargate-destroy
    Set Test Variable  ${example_path}  ${example_path}/fargate
    Terraform destroy  ${example_path}
    AWSession.spawn  eks  ${region}
    ${aws_api_resp}=  AWSession.get  list_clusters
    should not contain  ${aws_api_resp['clusters']}  ${eks_name}

# Test fargate and ec2
#     Set Test Variable  ${example_path}  ${example_path}/fargate_and_ec2
#     ${local_ip}=  Get AWS visible IP
#     Generate SSH key  ${example_path}  ssh-key
#     Terraform init    ${example_path}
#     Terraform get     ${example_path}
#     Terraform apply   ${example_path}  var={'PUBLIC_ACCESS_CIDRS':[\'${local_ip}/32\']}

#     ${terraform_resp}=  Terraform get json module values  ${example_path}  eks  aws_eks_cluster.cluster
#     Set suite variable  ${eks_name}  ${terraform_resp['values']['id']}

#     ${fargate_resp}=  Terraform get json module values  ${example_path}  fargate_default  aws_eks_fargate_profile.fargate
#     Set suite variable  ${fargate}  ${fargate_resp['values']['id']}

#     AWSession.spawn  eks  ${region}
#     ${aws_api_resp}=  AWSession.get  list_clusters
#     should contain  ${aws_api_resp['clusters']}  ${eks_name}

#     ${aws_api_resp}=  AWSession.get  list_fargate_profiles  clusterName=${eks_name}
#     should contain  ${aws_api_resp['fargateProfileNames']}  ${fargate}

#     Wait Until Keyword Succeeds  2 min  5 sec   Number of nodes should be  3
#     Wait Until Keyword Succeeds  2 min  5 sec   Number of pods should be  8

# Test fargate and ec2-destroy
#     Set Test Variable  ${example_path}  ${example_path}/fargate_and_ec2
#     Terraform destroy  ${example_path}
#     AWSession.spawn  eks  ${region}
#     ${aws_api_resp}=  AWSession.get  list_clusters
#     should not contain  ${aws_api_resp['clusters']}  ${eks_name}


# Test full
#     Set Test Variable  ${example_path}  ${example_path}/full
#     ${local_ip}=  Get AWS visible IP
#     Terraform init    ${example_path}
#     Terraform get     ${example_path}
#     Terraform apply   ${example_path}  var={'PUBLIC_ACCESS_CIDRS':[\'${local_ip}/32\']}

#     ${terraform_resp}=  Terraform get json module values  ${example_path}  eks  aws_eks_cluster.cluster
#     Set suite variable  ${eks_name}  ${terraform_resp['values']['id']}

#     ${fargate_resp}=  Terraform get json module values  ${example_path}  fargate_default  aws_eks_fargate_profile.fargate
#     Set suite variable  ${fargate_default}  ${fargate_resp['values']['id']}

#     ${fargate_resp}=  Terraform get json module values  ${example_path}  fargate_monitoring  aws_eks_fargate_profile.fargate
#     Set suite variable  ${fargate_monitoring}  ${fargate_resp['values']['id']}

#     AWSession.spawn  eks  ${region}
#     ${aws_api_resp}=  AWSession.get  list_clusters
#     should contain  ${aws_api_resp['clusters']}  ${eks_name}

#     ${aws_api_resp}=  AWSession.get  list_fargate_profiles  clusterName=${eks_name}
#     should contain  ${aws_api_resp['fargateProfileNames']}  ${fargate_monitoring}  ${fargate_default}

#     Wait Until Keyword Succeeds  2 min  5 sec   Number of nodes should be  3
#     Wait Until Keyword Succeeds  2 min  5 sec   Number of pods should be  8

# Test full-destroy
#     Set Test Variable  ${example_path}  ${example_path}/full
#     Terraform destroy  ${example_path}
#     AWSession.spawn  eks  ${region}
#     ${aws_api_resp}=  AWSession.get  list_clusters
#     should not contain  ${aws_api_resp['clusters']}  ${eks_name}

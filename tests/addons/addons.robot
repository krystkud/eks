*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite applies examples from given example_path, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the given example
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot
Resource  resources/kubernetes.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/addons
${region}       %{REGION}

*** Test Cases ***
Test simple
    Set Test Variable  ${example_path}  ${example_path}/simple
    ${local_ip}=  Get AWS visible IP
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}  var={'PUBLIC_ACCESS_CIDRS':[\'${local_ip}/32\']}

    ${terraform_resp}=  Terraform get json module values  ${example_path}  eks  aws_eks_cluster.cluster
    Set suite variable  ${name}  ${terraform_resp['values']['id']}

    AWSession.spawn  eks  ${region}
    ${aws_api_resp}=  AWSession.get  list_clusters
    should contain  ${aws_api_resp['clusters']}  ${name}

    Wait Until Keyword Succeeds  3 min  5 sec   Number of nodes should be  3
    Wait Until Keyword Succeeds  3 min  5 sec   Number of pods should be  8

    ${aws_api_vpc_cni_addon_resp}=  AWSession.get  describe_addon  clusterName=${name}  addonName=vpc-cni
    should be equal  ${aws_api_vpc_cni_addon_resp['addon']['status']}  ACTIVE

    ${aws_api_kube_proxy_addon_resp}=  AWSession.get  describe_addon  clusterName=${name}  addonName=kube-proxy
    should be equal  ${aws_api_kube_proxy_addon_resp['addon']['status']}  ACTIVE

    ${aws_api_coredns_addon_resp}=  AWSession.get  describe_addon  clusterName=${name}  addonName=coredns
    should be equal  ${aws_api_coredns_addon_resp['addon']['status']}  ACTIVE

Test simple-destroy
    Set Test Variable  ${example_path}  ${example_path}/simple
    Terraform destroy  ${example_path}
    AWSession.spawn  eks  ${region}
    ${aws_api_resp}=  AWSession.get  list_clusters
    should not contain  ${aws_api_resp['clusters']}  ${name}

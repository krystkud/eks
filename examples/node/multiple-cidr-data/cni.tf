resource "local_file" "eni_config" {
  count = length(module.networking.data_subnet_ids)
  content = templatefile("./templates/ENIConfig.yaml.tpl", {
    SUBNET = module.networking.data_subnet_ids[count.index],
    SG1    = module.networking.default_security_group_id
  })
  filename = "./templates/ENIConfig${count.index}.yaml"
}

resource "null_resource" "eni_config" {
  provisioner "local-exec" {
    command = <<EOF
    kubectl apply --kubeconfig ${module.eks.kubeconfig_filename} -f ./templates/ENIConfig.yaml
    kubectl --kubeconfig ${module.eks.kubeconfig_filename} set env daemonset aws-node -n kube-system AWS_VPC_K8S_CNI_CUSTOM_NETWORK_CFG=true
EOF
  }
  depends_on = [module.eks, module.eks_node]
  triggers = {
    cluster = module.eks.name
  }
}

resource "null_resource" "eni" {
  count = length(module.networking.data_subnet_ids)
  provisioner "local-exec" {
    command = <<EOF
    echo "Apply ENIConfig specific for the additional Subnets \n"
    kubectl apply --kubeconfig ${module.eks.kubeconfig_filename} -f ./templates/ENIConfig${count.index}.yaml
    sleep 50
    kubectl --kubeconfig ${module.eks.kubeconfig_filename} get nodes | awk '{print $1}' | grep -v NAME | sed -n '${count.index + 1}p' > ./templates/node.txt
    kubectl --kubeconfig ${module.eks.kubeconfig_filename} annotate node ${chomp(file("./templates/node.txt"))} k8s.amazonaws.com/eniConfig=${module.networking.data_subnet_ids[count.index]} --overwrite=true
    sleep 50
    kubectl apply --kubeconfig ${module.eks.kubeconfig_filename} -f ./templates/nginx.yaml
EOF
  }
  depends_on = [module.eks, module.eks_node, null_resource.eni_config]
  triggers = {
    cluster = module.eks.name
    run     = timestamp()
  }
}

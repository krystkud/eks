apiVersion: crd.k8s.amazonaws.com/v1alpha1
kind: ENIConfig
metadata:
 name: ${SUBNET}
spec:
  securityGroups:
    - ${SG1}
  subnet: ${SUBNET}

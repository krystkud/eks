AWS_REGION              = "us-east-2"
ACCOUNT_NAME            = "teribu"
VPC_CIDR_BLOCK          = "10.240.0.0/24"
PUBLIC_CIDR_BLOCKS      = ["10.240.0.0/27", "10.240.0.32/27", "10.11.192.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.240.0.64/27", "10.240.0.96/27", "10.240.0.128/27", "10.11.0.0/17", "10.11.128.0/18"]
ADDITIONAL_CIDRS        = ["10.11.0.0/16"]

resource "local_file" "eniconfig" {
  count = 2
  content = templatefile("./templates/ENIConfig.yaml.tpl", {
    SUBNET = module.networking.application_subnet_ids[count.index + 3],
    SG1    = module.networking.default_security_group_id
  })
  filename = "./templates/ENIConfig${count.index}.yaml"
}

resource "null_resource" "eni_config_setup" {
  provisioner "local-exec" {
    command = <<EOF
    kubectl apply --kubeconfig ${module.eks.kubeconfig_filename} -f ./templates/ENIConfig.yaml
    kubectl --kubeconfig ${module.eks.kubeconfig_filename} set env daemonset aws-node -n kube-system AWS_VPC_K8S_CNI_CUSTOM_NETWORK_CFG=true
EOF
  }
  depends_on = [module.eks, module.eks_node]
  triggers = {
    cluster = module.eks.name
  }
}

resource "null_resource" "eni_configs" {
  count = 2
  provisioner "local-exec" {
    command = <<EOF
    echo "Apply ENIConfig specific for the additional Subnets"
    kubectl apply --kubeconfig ${module.eks.kubeconfig_filename} -f ./templates/ENIConfig${count.index}.yaml
    sleep 50
    kubectl --kubeconfig ${module.eks.kubeconfig_filename} get nodes | awk '{print $1}' | grep -v NAME | sed -n '${count.index + 1}p' > ./templates/node.txt
    kubectl --kubeconfig ${module.eks.kubeconfig_filename} annotate node ${chomp(file("./templates/node.txt"))} k8s.amazonaws.com/eniConfig=${module.networking.application_subnet_ids[count.index + 3]} --overwrite=true
    sleep 50
    kubectl apply --kubeconfig ${module.eks.kubeconfig_filename} -f ./templates/nginx.yaml
EOF
  }
  depends_on = [module.eks, module.eks_node, null_resource.eni_config_setup]
  triggers = {
    cluster = module.eks.name
    run     = timestamp()
  }
}

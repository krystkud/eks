variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "PUBLIC_ACCESS_CIDRS" {
  description = "Provide cidr block which should have access to eks public endpoint"
  default     = ["194.145.235.0/24", "185.130.180.0/22", "85.89.187.155/32"]
  type        = list(string)
}

variable "ADDITIONAL_CIDRS" {
  default     = []
  description = "List of Additional CIDR Blocks for the VPC. Must be compatible with main CIDR, see https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html#vpc-resize"
  type        = list(string)
}

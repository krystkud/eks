variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "PUBLIC_KEY_PATH" {
  description = "Provide path to ssh public key"
  type        = string
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "Provide additional ssh public keys"
  default     = []
  type        = list(string)
}

variable "PUBLIC_ACCESS_CIDRS" {
  description = "Provide cidr block which should have access to eks public endpoint"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

output "info" {
  value = <<INFO
Cluster endpoint:
${module.eks.endpoint}

Interact with created cluster:
kubectl --kubeconfig ${module.eks.kubeconfig_filename} get pods --all-namespaces

List cluster nodes:
kubectl --kubeconfig ${module.eks.kubeconfig_filename} get nodes

Bastion host:
${module.bastion.bastion_dns_name}

INFO
}

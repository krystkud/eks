provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = random_string.env.id
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "audit_logs" {
  source          = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/logs?ref=master"
  globals         = module.common.globals
  KMS_KEY_ID      = module.encryption.kms_key_id
  EXPIRATION_DAYS = 1
  FORCE_DESTROY   = true
  BUCKET_NAME     = random_pet.pet.id
}

module "dns_zone" {
  source                  = "git::https://bitbucket.org/digitallabsbuild/dns.git//modules/zone_public?ref=master"
  globals                 = module.common.globals
  DNS_ZONE_NAME           = "${random_string.env.id}.cat-test.idemia.io"
  CREATE_ZONE_CERTIFICATE = true
}

module "delegation" {
  source        = "git::https://bitbucket.org/digitallabsbuild/dns.git//modules/delegation?ref=master"
  NAME_SERVERS  = module.dns_zone.public_name_servers
  DNS_ZONE_NAME = module.dns_zone.zone_name
  ZONE_ID       = "ZYM997DTBGCL3" # cat-test.idemia.io @ teribu
}

module "dns_local_zone" {
  source        = "git::https://bitbucket.org/digitallabsbuild/dns.git//modules/zone_private?ref=master"
  globals       = module.common.globals
  networking    = module.networking.common
  DNS_ZONE_NAME = "${random_pet.pet.id}.cat-test.idemia.io"
}

module "networking" {
  source                  = "git::https://bitbucket.org/digitallabsbuild/vpc.git//modules/networking?ref=master"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = true
  CREATE_PRIVATE_DNS_ZONE = false
}

module "ingress_alb" {
  source                   = "git::https://bitbucket.org/digitallabsbuild/ingress.git//modules/alb?ref=master"
  globals                  = module.common.globals
  networking               = module.networking.common
  PUBLIC_SUBNET_IDS        = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN = module.dns_zone.wildcard_certificate_arn
  INGRESS_ACCESS_CIDR      = var.PUBLIC_ACCESS_CIDRS
  BALANCER_IDLE_TIMEOUT    = 1800
  INTERNAL                 = false
}

module "local_ingress_alb" {
  source                   = "git::https://bitbucket.org/digitallabsbuild/ingress.git//modules/alb?ref=master"
  globals                  = module.common.globals
  networking               = module.networking.common
  PUBLIC_SUBNET_IDS        = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN = module.dns_zone.wildcard_certificate_arn
  INTERNAL                 = true
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source                    = "../../../modules/cluster"
  globals                   = module.common.globals
  networking                = module.networking.common
  SUBNET_IDS                = module.networking.application_subnet_ids
  KMS_KEY_ID                = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS       = var.PUBLIC_ACCESS_CIDRS
  CREATE_KUBECONFIG         = true
  ENABLE_FARGATE            = true
  ENABLED_CLUSTER_LOG_TYPES = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  ADMIN_USERS_ARN           = ["arn:aws:iam::737020658184:user/cpt-jenkins"]
  ADMIN_ROLES_ARN           = ["arn:aws:iam::737020658184:role/cpt-admin"]
}

module "fargate_default" {
  source            = "../../../modules/fargate_nodes/"
  globals           = module.common.globals
  networking        = module.networking.common
  CLUSTER_NAME      = module.eks.name
  SUBNET_IDS        = module.networking.application_subnet_ids
  FARGATE_NAMESPACE = "default"
  SUFFIX            = "default"
  DEPENDS_ON_MODULE = [module.eks]
}

resource "null_resource" "reload" {

  depends_on = [module.eks, module.fargate_default, module.fargate_monitoring]

  provisioner "local-exec" {
    command = <<EOF

echo "Delete and re-create any existing pods so that they are scheduled on Fargate";
kubectl --kubeconfig kubeconfig.conf patch deployment coredns -n kube-system --type json -p='[{"op": "remove", "path": "/spec/template/metadata/annotations/eks.amazonaws.com~1compute-type"}]'
kubectl --kubeconfig kubeconfig.conf rollout restart -n kube-system deployment coredns
EOF
  }
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.cluster.token
    load_config_file       = false
  }
}

module "fargate_monitoring" {
  source            = "../../../modules/fargate_nodes/"
  globals           = module.common.globals
  networking        = module.networking.common
  CLUSTER_NAME      = module.eks.name
  SUBNET_IDS        = module.networking.application_subnet_ids
  FARGATE_NAMESPACE = "monitoring"
  SUFFIX            = "monitoring"
  DEPENDS_ON_MODULE = [module.fargate_default]
}

module "grafana_dashboards" {
  source = "git::https://bitbucket.org/digitallabsbuild/kubernetes.git//modules/grafana-dashboards?ref=master"
}

module "grafana_datasource" {
  source = "git::https://bitbucket.org/digitallabsbuild/kubernetes.git//modules/grafana-datasource?ref=master"
}

module "prometheus_monitoring" {
  source                          = "git::https://bitbucket.org/digitallabsbuild/kubernetes.git//modules/prometheus?ref=master"
  KUBECONFIG                      = module.eks.kubeconfig_filename
  PROMETHEUS_DEPLOYMENT_NAMESPACE = "monitoring"
  PROMETHEUS_EXTERNAL_LABEL       = random_pet.pet.id
  NODE_EXPORTER_ENABLED           = false
  PROMETHEUS_INGRESS              = false
  ZONE_ID                         = module.dns_zone.zone_id
  ALIAS_NAME                      = module.ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID                   = module.ingress_alb.ingress_zone_id
  GRAFANA_INGRESS_WHITELIST       = var.PUBLIC_ACCESS_CIDRS
  DEPENDS_ON_MODULE               = [module.fargate_monitoring]
}

# module "fargate_logging" {
#   source            = "../../../modules/fargate_nodes/"
#   globals           = module.common.globals
#   networking        = module.networking.common
#   CLUSTER_NAME      = module.eks.name
#   SUBNET_IDS        = module.networking.application_subnet_ids
#   FARGATE_NAMESPACE = "logging"
#   SUFFIX            = "logging"
#   DEPENDS_ON_MODULE = [module.fargate_monitoring]
# }

# module "loki_dynamodb_table" {
#   source          = "git::https://bitbucket.org/digitallabsbuild/dynamodb.git//modules?ref=master"
#   globals         = module.common.globals
#   TABLE_NAME      = "${var.PROJECT}-${var.ENVIRONMENT}-loki"
#   KEY_COLUMN_NAME = "h"
#   RANGE_KEY       = "r"
#   RANGE_KEY_TYPE  = "B"
#   READ_CAPACITY   = 10
#   WRITE_CAPACITY  = 10
# }

# module "logging_operator" {
#   source            = "git::https://bitbucket.org/digitallabsbuild/logging.git//modules/eks_logging?ref=master"
#   globals           = module.common.globals
#   KUBECONFIG        = module.eks.kubeconfig_filename
#   DEPENDS_ON_MODULE = [module.fargate_logging]
# }

# module "loki_logging" {
#   source                    = "git::https://bitbucket.org/digitallabsbuild/logging.git//modules/eks_loki?ref=master"
#   globals                   = module.common.globals
#   KUBECONFIG                = module.eks.kubeconfig_filename
#   LOKI_DEPLOYMENT_NAMESPACE = "monitoring"
#   DEPENDS_ON_MODULE         = [module.fargate_logging]
# }

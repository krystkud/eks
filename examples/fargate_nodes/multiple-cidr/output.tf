output "info" {
  value = <<INFO
Cluster endpoint:
${module.eks.endpoint}

Interact with created cluster:
kubectl --kubeconfig ${module.eks.kubeconfig_filename} get pods --all-namespaces
Keep in mind that Fargate will launch 10 pods at a time, so you'll have to wait a bit before seeing them all

Generate kubeconfig:
aws eks --region ${var.AWS_REGION} update-kubeconfig --name ${module.eks.name}


INFO
}

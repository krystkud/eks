provider "aws" {
  region = var.AWS_REGION
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = random_string.env.id
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "networking" {
  source                  = "git::https://bitbucket.org/digitallabsbuild/vpc.git//modules/networking?ref=master"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = true
  CREATE_PRIVATE_DNS_ZONE = false
}

module "eks" {
  source              = "../../../modules/cluster"
  globals             = module.common.globals
  networking          = module.networking.common
  SUBNET_IDS          = module.networking.application_subnet_ids
  KMS_KEY_ID          = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS = var.PUBLIC_ACCESS_CIDRS
  CREATE_KUBECONFIG   = true
  ENABLE_FARGATE      = true
}

module "fargate_default" {
  source            = "../../../modules/fargate_nodes/"
  globals           = module.common.globals
  CLUSTER_NAME      = module.eks.name
  SUBNET_IDS        = module.networking.application_subnet_ids
  FARGATE_NAMESPACE = "default"
  SUFFIX            = "default"
  DEPENDS_ON_MODULE = [module.eks]
}

resource "null_resource" "reload" {

  depends_on = [module.eks, module.fargate_default]

  provisioner "local-exec" {
    command = <<EOF

echo "Delete and re-create any existing pods so that they are scheduled on Fargate";
kubectl --kubeconfig kubeconfig.conf patch deployment coredns -n kube-system --type json -p='[{"op": "remove", "path": "/spec/template/metadata/annotations/eks.amazonaws.com~1compute-type"}]'
kubectl --kubeconfig kubeconfig.conf rollout restart -n kube-system deployment coredns
EOF
  }
}

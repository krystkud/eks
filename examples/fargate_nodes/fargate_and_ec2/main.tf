provider "aws" {
  region = var.AWS_REGION
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = random_string.env.id
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "networking" {
  source                  = "git::https://bitbucket.org/digitallabsbuild/vpc.git//modules/networking?ref=master"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = true
  CREATE_PRIVATE_DNS_ZONE = false
}

module "eks" {
  source              = "../../../modules/cluster"
  globals             = module.common.globals
  networking          = module.networking.common
  SUBNET_IDS          = module.networking.application_subnet_ids
  KMS_KEY_ID          = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS = var.PUBLIC_ACCESS_CIDRS
  CREATE_KUBECONFIG   = true
  ENABLE_FARGATE      = true
  FARGATE_NAMESPACE   = "monitoring"
  ADMIN_USERS_ARN     = ["arn:aws:iam::737020658184:user/cpt-jenkins"]
  ADMIN_ROLES_ARN     = ["arn:aws:iam::737020658184:role/cpt-admin"]
}

module "eks_node" {
  source             = "../../../modules/node"
  cluster            = module.eks.cluster
  globals            = module.common.globals
  KMS_KEY_ID         = module.encryption.kms_key_id
  SUBNET_IDS         = module.networking.application_subnet_ids
  SECURITY_GROUPS_ID = [module.networking.default_security_group_id]
  DEPENDS_ON_MODULE  = [module.eks]
}

module "fargate_default" {
  source            = "../../../modules/fargate_nodes/"
  globals           = module.common.globals
  networking        = module.networking.common
  CLUSTER_NAME      = module.eks.name
  SUBNET_IDS        = module.networking.application_subnet_ids
  FARGATE_NAMESPACE = "default"
  SUFFIX            = "default"
  DEPENDS_ON_MODULE = [module.eks]
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.cluster.token
    load_config_file       = false
  }
}

module "prometheus_monitoring" {
  source                    = "git::https://bitbucket.org/digitallabsbuild/kubernetes.git//modules/prometheus?ref=master"
  KUBECONFIG                = module.eks.kubeconfig_filename
  PROMETHEUS_EXTERNAL_LABEL = "fargate"
  NODE_EXPORTER_ENABLED     = false
  DEPENDS_ON_MODULE         = [module.eks]
}

module "ssm" {
  source    = "git::https://bitbucket.org/digitallabsbuild/iam.git//modules/role_policy_attachment?ref=master"
  ROLE_NAME = module.eks.node_role_name
  POLICIES  = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

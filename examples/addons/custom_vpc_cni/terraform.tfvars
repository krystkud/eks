AWS_REGION              = "eu-central-1"
ACCOUNT_NAME            = "teribu"
VPC_CIDR_BLOCK          = "10.10.0.0/16"
PUBLIC_CIDR_BLOCKS      = ["10.10.0.0/24", "10.10.1.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.10.10.0/24", "10.10.11.0/24"]
AMI_OWNER               = "668392192854"
KMS_SHARED_AMI_KEY_ID   = "arn:aws:kms:eu-central-1:668392192854:key/152b6fae-c57b-4a93-a573-2b8af8bd0888"
KUBERNETES_VERSION      = "1.21"
CLUSTER_ADDONS = {
  vpc-cni = {
    addon_version     = "v1.10.1-eksbuild.1"
    resolve_conflicts = "OVERWRITE"
  }
  coredns = {
    addon_version     = "v1.8.4-eksbuild.1"
    resolve_conflicts = "OVERWRITE"
  }
  kube-proxy = {
    addon_version     = "v1.21.2-eksbuild.2"
    resolve_conflicts = "OVERWRITE"
  }
}

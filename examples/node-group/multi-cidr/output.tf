output "info" {
  value = <<OUTPUT

CLUSTER NAME              :: ${module.eks.name}
REGION                    :: ${var.AWS_REGION}

VPC_CIDR_BLOCK :: "${var.VPC_CIDR_BLOCK}"

Interact with created cluster:
kubectl --kubeconfig ${module.eks.kubeconfig_filename} get pods --all-namespaces

List cluster nodes:
kubectl --kubeconfig ${module.eks.kubeconfig_filename} get nodes

Copy kubeconfig to your home:
cp kubeconfig.conf ~/.kube/config

Generate kubeconfig:
aws eks --region ${var.AWS_REGION} update-kubeconfig --name ${module.eks.name}

OUTPUT
}


output "arn" {
  value = module.node_group.arn
}

output "id" {
  value = module.node_group.id
}

output "resources" {
  value = module.node_group.resources
}

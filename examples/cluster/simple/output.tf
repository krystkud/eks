output "info" {
  value = <<INFO
Cluster endpoint:
${module.eks.endpoint}

Interact with created cluster:
kubectl --kubeconfig ${module.eks.kubeconfig_filename} get pods --all-namespaces
INFO
}

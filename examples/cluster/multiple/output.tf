output "info" {
  value = <<INFO
Cluster endpoint:

${module.eks_marine.endpoint}

${module.eks_wife.endpoint}

Interact with created cluster:
kubectl --kubeconfig ${module.eks_marine.kubeconfig_filename} get pods --all-namespaces
kubectl --kubeconfig ${module.eks_wife.kubeconfig_filename} get pods --all-namespaces

INFO
}

variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "PUBLIC_ACCESS_CIDRS" {
  description = "Provide cidr block which should have access to eks public endpoint"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "ADMIN_ROLES_ARN" {
  default     = []
  type        = list(string)
  description = "Provide list of the roles arn with admin right"
}

variable "ADMIN_USERS_ARN" {
  default     = []
  type        = list(string)
  description = "Provide list of the users arn with admin right"
}
variable "AMI_OWNER" {
  default     = ""
  description = "Provide ami owner"
  type        = string
}

variable "KMS_SHARED_AMI_KEY_ID" {
  description = "Provide kms shared key id used to encrypt root partition of shared AMIs"
  type        = string
}

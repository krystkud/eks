provider "aws" {
  region = var.AWS_REGION
}

data "aws_eks_cluster_auth" "marine" {
  name = module.eks_marine.name
}

data "aws_eks_cluster" "marine" {
  name = module.eks_marine.name
}

provider "kubernetes" {
  alias                  = "marine"
  host                   = data.aws_eks_cluster.marine.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.marine.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.marine.token
}

data "aws_eks_cluster_auth" "wife" {
  name       = module.eks_wife.name
  depends_on = [module.eks_wife]
}

data "aws_eks_cluster" "wife" {
  name       = module.eks_wife.name
  depends_on = [module.eks_wife]
}

provider "kubernetes" {
  alias                  = "wife"
  host                   = data.aws_eks_cluster.wife.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.wife.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.wife.token
  load_config_file       = false
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source                = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME          = var.ACCOUNT_NAME
  AWS_REGION            = var.AWS_REGION
  ENVIRONMENT           = random_string.env.id
  PROJECT               = random_pet.pet.id
  AMI_OWNER             = var.AMI_OWNER
  KMS_SHARED_AMI_KEY_ID = var.KMS_SHARED_AMI_KEY_ID
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "networking" {
  source                  = "git::https://bitbucket.org/digitallabsbuild/vpc.git//modules/networking?ref=master"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = true
  CREATE_PRIVATE_DNS_ZONE = false
}

module "eks_wife" {
  source                    = "../../../modules/cluster"
  globals                   = module.common.globals
  networking                = module.networking.common
  SUBNET_IDS                = module.networking.application_subnet_ids
  KMS_KEY_ID                = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS       = var.PUBLIC_ACCESS_CIDRS
  SUFFIX                    = "wife-${random_string.env.id}"
  KUBECONFIG_FILENAME       = "kube_wife.conf"
  ADMIN_ROLES_ARN           = var.ADMIN_ROLES_ARN
  ADMIN_USERS_ARN           = var.ADMIN_USERS_ARN
  ENABLED_CLUSTER_LOG_TYPES = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
}

module "wife_node" {
  source             = "../../../modules/node"
  cluster            = module.eks_wife.cluster
  globals            = module.common.globals
  KMS_KEY_ID         = module.encryption.kms_key_id
  SUBNET_IDS         = module.networking.application_subnet_ids
  SECURITY_GROUPS_ID = [module.networking.default_security_group_id]
  NR_OF_NODES        = 1
  SUFFIX             = "wife-${random_string.env.id}"
  CLUSTER_SUFFIX     = "wife-${random_string.env.id}"
  depends_on         = [module.eks_wife, data.aws_eks_cluster.wife]
}

module "wives_node" {
  source             = "../../../modules/node"
  cluster            = module.eks_wife.cluster
  globals            = module.common.globals
  KMS_KEY_ID         = module.encryption.kms_key_id
  SUBNET_IDS         = module.networking.application_subnet_ids
  SECURITY_GROUPS_ID = [module.networking.default_security_group_id]
  NR_OF_NODES        = 1
  SUFFIX             = "wives-${random_string.env.id}"
  CLUSTER_SUFFIX     = "wife-${random_string.env.id}"
  depends_on         = [module.eks_wife, data.aws_eks_cluster.wife]
}

module "eks_marine" {
  source              = "../../../modules/cluster"
  globals             = module.common.globals
  networking          = module.networking.common
  SUBNET_IDS          = module.networking.application_subnet_ids
  KMS_KEY_ID          = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS = var.PUBLIC_ACCESS_CIDRS
  SUFFIX              = "marine-${random_string.env.id}"
  KUBECONFIG_FILENAME = "kube_marine.conf"
  ADMIN_ROLES_ARN     = var.ADMIN_ROLES_ARN
  ADMIN_USERS_ARN     = var.ADMIN_USERS_ARN
}

module "marine_node" {
  source             = "../../../modules/node"
  cluster            = module.eks_marine.cluster
  globals            = module.common.globals
  KMS_KEY_ID         = module.encryption.kms_key_id
  SUBNET_IDS         = module.networking.application_subnet_ids
  SECURITY_GROUPS_ID = [module.networking.default_security_group_id]
  NR_OF_NODES        = 1
  SUFFIX             = "marine-${random_string.env.id}"
  CLUSTER_SUFFIX     = "marine-${random_string.env.id}"
  depends_on         = [module.eks_marine, data.aws_eks_cluster.marine]
}

module "ssm_wife" {
  source    = "git::https://bitbucket.org/digitallabsbuild/iam.git//modules/role_policy_attachment?ref=master"
  ROLE_NAME = module.eks_wife.node_role_name
  POLICIES  = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

module "ssm_marine" {
  source    = "git::https://bitbucket.org/digitallabsbuild/iam.git//modules/role_policy_attachment?ref=master"
  ROLE_NAME = module.eks_marine.node_role_name
  POLICIES  = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

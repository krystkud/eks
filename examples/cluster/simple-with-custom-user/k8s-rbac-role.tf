resource "kubernetes_cluster_role" "cluster_role" {
  depends_on = [module.eks]
  metadata {
    name = "sample-role"
  }
  rule {
    api_groups = [""]
    resources  = ["pods", "configmaps", "secrets", "endpoints", "services", "pods/log", "pods/eviction"]
    verbs      = ["create", "get", "update", "patch", "list", "delete"]
  }
}

resource "kubernetes_role_binding" "role_binding" {
  depends_on = [kubernetes_cluster_role.cluster_role]
  metadata {
    name      = "sample-role"
    namespace = "kube-system"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.cluster_role.metadata[0].name
  }
  subject {
    kind      = "Group"
    name      = var.SAMPLE_RBAC_GROUP_NAME
    namespace = "kube-system"
  }
}
